/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

(function(global) {
    'use strict';
    // @see http://phrogz.net/JS/Classes/OOPinJS2.html

    var Ontology = function () {
        this.prefixes = {};
        this.classes = {};
        this.properties = {};
        this.individuals = {};
    };

    /**
     * Load ontology from JSON obj
     *
     * @param obj
     * @returns Ontology
     */
    Ontology.fromJson = function (obj) {
        var ontology = new Ontology();
        ontology.prefixes = obj['@context'];

        for (var uri in obj.classes) {
            var _class = ontology.addClass(new OntologyClass(ontology, uri));
            _class.populateFromJson(obj.classes[uri]);
        }

        for (var uri in obj.properties) {
            var property = ontology.addProperty(new OntologyProperty(ontology, uri));
            property.populateFromJson(obj.properties[uri]);
        }

        for (var uri in obj.individuals) {
            var individual = ontology.addIndividual(new OntologyIndividual(ontology, uri));
            individual.populateFromJson(obj.individuals[uri]);
        }

        return ontology;
    };

    Ontology.prototype = {

        /**
         * Expand an uri
         *
         * @param uri
         * @returns string
         */
        expandPrefix: function (uri) {
            var pos = uri.indexOf(":");
            if (pos > -1 && uri.indexOf(" ") < 0) {
                var prefix = uri.substring(0, pos);
                if (this.prefixes[prefix]) {
                    return this.prefixes[prefix] + uri.substring(pos + 1);
                }
            }
            return uri;
        },

        /**
         * URI short form
         *
         * @param uri
         * @returns string
         */
        shortForm: function (uri) {
            for (var key in this.prefixes) {
                if (uri.indexOf(this.prefixes[key]) == 0) {
                    return key + ':' + uri.substr(this.prefixes[key].length);
                }
            }
            return uri;
        },

        /**
         * Get prefixes
         *
         * @returns {{}|*}
         */
        getPrefixes: function () {
            return this.prefixes;
        },

        /**
         * Get classes
         *
         * @returns {OntologyClass}
         */
        getClasses: function () {
            return this.classes;
        },

        /**
         * Get class
         *
         * @param uri
         * @returns OntologyClass
         */
        getClass: function (uri) {
            uri = this.expandPrefix(uri);
            return this.classes[uri] ? this.classes[uri] : null;
        },

        /**
         * Add a class
         *
         * @param _class OntologyClass
         * @returns OntologyClass
         */
        addClass: function (_class) {
            if (!this.classes[_class.getUri()]) {
                this.classes[_class.getUri()] = _class;
            }
            return this.classes[_class.getUri()];
        },

        /**
         * Normalize a class uri
         *
         * @param res
         * @returns OntologyClass
         */
        normalizeClass: function (res) {
            if (!(res instanceof OntologyClass)) {
                return this.getClass(res);
            }
            return res;
        },

        /**
         * Get properties
         *
         * @returns {OntologyProperty}
         */
        getProperties: function () {
            return this.properties;
        },

        /**
         * Get property
         *
         * @param uri
         * @returns OntologyProperty
         */
        getProperty: function (uri) {
            uri = this.expandPrefix(uri);
            return this.properties[uri] ? this.properties[uri] : null;
        },

        /**
         * Add property
         *
         * @param property
         * @returns OntologyProperty
         */
        addProperty: function (property) {
            if (!this.properties[property.getUri()]) {
                this.properties[property.getUri()] = property;
            }
            return this.properties[property.getUri()];
        },

        /**
         * Normalize property uri
         * @param res
         * @returns OntologyProperty
         */
        normalizeProperty: function (res) {
            if (!(res instanceof OntologyProperty)) {
                return this.getProperty(res);
            }
            return res;
        },

        /**
         * Get individuals
         *
         * @returns {OntologyIndividual}
         */
        getIndividuals: function () {
            return this.individuals;
        },

        /**
         * Get individual
         *
         * @param uri
         * @returns OntologyIndividual
         */
        getIndividual: function (uri) {
            uri = this.expandPrefix(uri);
            return this.individuals[uri] ? this.individuals[uri] : null;
        },

        /**
         * Add individual
         *
         * @param individual
         * @returns OntologyIndividual
         */
        addIndividual: function (individual) {
            if (!this.individuals[individual.getUri()]) {
                this.individuals[individual.getUri()] = individual;
            }
            return this.individuals[individual.getUri()];
        },

        /**
         * Normalize individual uri
         * @param res
         * @returns OntologyIndividual
         */
        normalizeIndividual: function (res) {
            if (!(res instanceof OntologyIndividual)) {
                return this.getIndividual(res);
            }
            return res;
        }
    };

    // -----------------------
    // RESOURCE
    // -----------------------

    /**
     * OntologyResource constructor
     *
     * @param ontology
     * @param uri
     * @constructor
     */
    var OntologyResource = function (ontology, uri) {
        this.ontology = ontology;
        this.uri = ontology.expandPrefix(uri);
        this.label = null;
        this.comment = null;
    };

    OntologyResource.prototype = {

        /**
         * Get ontology
         *
         * @returns Ontology
         */
        getOntology: function () {
            return this.ontology;
        },

        /**
         * Get uri
         *
         * @returns string
         */
        getUri: function () {
            return this.uri;
        },

        /**
         * Get short uri
         *
         * @returns string
         */
        getShortUri: function () {
            return this.ontology.shortForm(this.uri);
        },

        /**
         * Has same uri
         *
         * @param uri
         * @returns boolean
         */
        hasUri: function (uri) {
            return this.uri == this.ontology.expandPrefix(uri);
        },

        /**
         * Get label
         *
         * @returns string
         */
        getLabel: function () {
            return this.label;
        },

        /**
         * Get comment
         *
         * @returns string
         */
        getComment: function () {
            return this.comment;
        },

        /**
         * Get prefered label (label or short uri)
         *
         * @returns string
         */
        getPreferedLabel: function () {
            return this.label ? this.label : this.getShortUri();
        },

        /**
         * toString : full uri
         *
         * @returns string
         */
        toString: function () {
            return this.uri;
        },

        /**
         * Test resource equality
         *
         * @param resource
         * @returns {boolean}
         */
        equals: function (resource) {
            var result = false;
            if (resource instanceof OntologyResource) {
                result = this.hasUri(resource.getUri());
            }
            return result;
        }
    };

    // -----------------------
    // CLASS
    // -----------------------

    /**
     * OntologyClass constructor
     *
     * @param ontology
     * @param uri
     * @constructor
     */
    var OntologyClass = function (ontology, uri) {
        this.superClasses = [];
        this.subClasses = [];
        this.directProperties = [];
        this.candidateProperties = [];
        this.thesaurus = null;
        OntologyResource.call(this, ontology, uri); // call parent constructor
    };
    OntologyClass.prototype = Object.create(OntologyResource.prototype);
    OntologyClass.prototype.constructor = OntologyClass;

    /**
     * Populate from JSON
     *
     * @param obj
     */
    OntologyClass.prototype.populateFromJson = function (obj) {
        var that = this;
        this.label = obj.label || null;
        this.comment = obj.comment || null;
        this.thesaurus = obj.thesaurus || false;

        // super / sub classes
        ["superClasses", "subClasses"].forEach(function (c) {
            if (obj[c]) {
                for (var i = 0; i < obj[c].length; i++) {
                    var _class = new OntologyClass(that.getOntology(), obj[c][i]);
                    that[c].push(that.getOntology().addClass(_class));
                }
            }
        });

        // direct / candidate properties
        if (obj.transversalProperties) {
            obj.candidateProperties = obj.transversalProperties;
        }
        ["directProperties", "candidateProperties"].forEach(function (c) {
            if (obj[c]) {
                for (var i = 0; i < obj[c].length; i++) {
                    var property = new OntologyProperty(that.getOntology(), obj[c][i]);
                    that[c].push(that.getOntology().addProperty(property));
                }
            }
        });
    };

    /**
     * Get super classes
     *
     * @param direct
     * @returns [OntologyClass]
     */
    OntologyClass.prototype.getSuperClasses = function (direct) {
        direct = direct !== undefined ? direct : true;
        if (direct) {
            return this.superClasses;
        } else {
            var classes = [].concat(this.superClasses);
            for (var i = 0; i < this.superClasses.length; i++) {
                classes = classes.concat(this.superClasses[i].getSuperClasses(false));
            }
            return classes;
        }
    };

    /**
     * Get sub classes
     *
     * @param direct
     * @returns OntologyClass[]
     */
    OntologyClass.prototype.getSubClasses = function (direct) {
        direct = direct !== undefined ? direct : true;
        if (direct) {
            return this.subClasses;
        } else {
            var classes = [].concat(this.subClasses);
            for (var i = 0; i < this.subClasses.length; i++) {
                classes = classes.concat(this.subClasses[i].getSubClasses(false));
            }
            return classes;
        }
    };

    /**
     * Get direct properties
     *
     * @returns OntologyProperty[]
     */
    OntologyClass.prototype.getDirectProperties = function () {
        return this.directProperties;
    };

    /**
     * Get candidate properties
     *
     * @returns OntologyProperty[]
     */
    OntologyClass.prototype.getCandidateProperties = function () {
        return this.candidateProperties;
    };

    /**
     * Is thesaurus class ?
     *
     * @return boolean
     */
    OntologyClass.prototype.isThesaurus = function () {
        return this.thesaurus;
    };

    /**
     * Return true if class is a subclass of given resource
     *
     * @param res OntologyClass
     * @param direct
     * @return boolean
     */
    OntologyClass.prototype.isSubClassOf = function (res, direct) {
        direct = direct !== undefined ? direct : true;
        res = this.ontology.normalizeClass(res);
        if (!res) {
            return false;
        }
        if (res.hasUri(this.getUri())) {
            return true;
        }
        return res.getSubClasses(direct).indexOf(this) > -1;
    };

    /**
     * Return true if class is a superclass of given resource
     *
     * @param res OntologyClass
     * @param direct
     * @return boolean
     */
    OntologyClass.prototype.isSuperClassOf = function (res, direct) {
        direct = direct !== undefined ? direct : true;
        res = this.ontology.normalizeClass(res);
        if (!res) {
            return false;
        }
        if (res.hasUri(this.getUri())) {
            return true;
        }
        return res.getSuperClasses(direct).indexOf(this) > -1;
    };

    /**
     * Return true if the given class is transversal to res (equal, equal to transitive superclass,
     * equal to transitive subclass, equal to transitive superclass of transitive subclass)
     *
     * @param res OntologyClass
     * @param processSubClasses
     * @return boolean
     */
    OntologyClass.prototype.isCandidateClassOf = function (res, skipSubClasses) {
        res = this.ontology.normalizeClass(res);
        if (!res) {
            return false;
        }
        if (res.hasUri(this.getUri())) {
            return true;
        }

        var superClasses = res.getSuperClasses(true);
        for (var i = 0; i < superClasses.length; i++) {
            if (this.isCandidateClassOf(superClasses[i], true)) {
                return true;
            }
        }

        if (!skipSubClasses) {
            var subClasses = res.getSubClasses(true);
            for (var i = 0; i < subClasses.length; i++) {
                if (this.isCandidateClassOf(subClasses[i])) {
                    return true;
                }
            }
        }

        return false;
    };

    /**
     * Get individuals
     *
     * @param direct
     * @returns OntologyIndividual[]
     */
    OntologyClass.prototype.getIndividuals = function (direct) {
        direct = direct !== undefined ? direct : true;
        var individuals = [];
        var _ind = this.ontology.getIndividuals();
        for (var uri in _ind) {
            if (_ind[uri].getTypes(direct).indexOf(this) > -1) {
                individuals.push(_ind[uri]);
            }
        }
        return individuals;
    };

    // -----------------------
    // PROPERTY
    // -----------------------

    /**
     * OntologyProperty constructor
     *
     * @param ontology
     * @param uri
     * @constructor
     */
    var OntologyProperty = function (ontology, uri) {
        OntologyResource.call(this, ontology, uri); // call parent constructor
        this.type = null;
        this.functional = false;
        this.priority = 0;
        this.ranges = [];
        this.directDomains = [];
    };
    OntologyProperty.prototype = Object.create(OntologyResource.prototype);
    OntologyProperty.prototype.constructor = OntologyProperty;

    /**
     * Populate from JSON
     *
     * @param obj
     */
    OntologyProperty.prototype.populateFromJson = function (obj) {
        var that = this;
        this.label = obj.label || null;
        this.comment = obj.comment || null;
        this.type = obj.type || null;
        this.functional = obj.functional || false;
        this.priority = obj.priority || 0;

        // ranges / directDomains
        ["ranges", "directDomains"].forEach(function (c) {
            if (obj[c]) {
                for (var i = 0; i < obj[c].length; i++) {
                    var _class = new OntologyClass(that.getOntology(), obj[c][i]);
                    that[c].push(that.getOntology().addClass(_class));
                }
            }
        });
    };

    /**
     * Get type
     *
     * @return string
     */
    OntologyProperty.prototype.getType = function () {
        return this.type;
    };

    /**
     * Is functional ?
     *
     * @return boolean;
     */
    OntologyProperty.prototype.isFunctional = function () {
        return this.functional;
    };

    /**
     * Get priority
     *
     * @return number
     */
    OntologyProperty.prototype.getPriority = function () {
        return this.priority;
    };

    /**
     * Get ranges
     *
     * @return OntologyClass[]
     */
    OntologyProperty.prototype.getRanges = function () {
        return this.ranges;
    };

    /**
     * Get range
     *
     * @return OntologyClass
     */
    OntologyProperty.prototype.getRange = function () {
        return this.ranges.length ? this.ranges[0] : null;
    };

    /**
     * Get direct domains
     *
     * @return OntologyClass[]
     */
    OntologyProperty.prototype.getDirectDomains = function () {
        return this.directDomains;
    };

    // -----------------------
    // INDIVIDUAL
    // -----------------------

    /**
     * OntologyIndividual constructor
     *
     * @param ontology
     * @param uri
     * @constructor
     */
    var OntologyIndividual = function (ontology, uri) {
        OntologyResource.call(this, ontology, uri); // call parent constructor
        this.types = [];
        this.propertiesValues = {};
    };
    OntologyIndividual.prototype = Object.create(OntologyResource.prototype);
    OntologyIndividual.prototype.constructor = OntologyIndividual;

    /**
     * Populate from JSON
     *
     * @param obj
     */
    OntologyIndividual.prototype.populateFromJson = function (obj) {
        var that = this;
        this.label = obj.label || null;
        this.comment = obj.comment || null;

        if (obj.types) {
            for (var i = 0; i < obj.types.length; i++) {
                var _class = new OntologyClass(that.getOntology(), obj.types[i]);
                this.types.push(that.getOntology().addClass(_class));
            }
        }

        if (obj.properties) {
            for (var uri in obj.properties) {
                this.propertiesValues[this.ontology.expandPrefix(uri)] = obj.properties[uri];
            }
        }
    };

    /**
     * Get types
     *
     * @param direct
     * @returns OntologyClass[]
     */
    OntologyIndividual.prototype.getTypes = function (direct) {
        direct = direct !== undefined ? direct : true;
        if (direct) {
            return this.types;
        } else {
            var types = [].concat(this.types);
            for (var i = 0; i < this.types.length; i++) {
                types = types.concat(this.types[i].getSuperClasses(false));
            }
            return types;
        }
    };

    /**
     * Get type
     *
     * @param direct
     * @returns OntologyClass[]
     */
    OntologyIndividual.prototype.getType = function (direct) {
        var types = this.getTypes(direct);
        return types.length ? types[0] : null;
    };

    /**
     * Get properties values
     *
     * @returns {{}|*}
     */
    OntologyIndividual.prototype.getPropertiesValues = function () {
        return this.propertiesValues;
    };

    /**
     * Get property values by URI
     *
     * @param uri
     * @returns {*|Array}
     */
    OntologyIndividual.prototype.getPropertyValues = function (uri) {
        uri = this.ontology.expandPrefix(uri);
        return this.propertiesValues[uri] || [];
    };

    // -----------------------
    // EXPORT
    // -----------------------

    if (typeof define === 'function' && define.amd) {
        define(function () {
            return Ontology;
        });
    } else if (typeof exports === 'object') {
        module.exports = Ontology;
    } else {
        global.Ontology = Ontology;
    }

})(this);